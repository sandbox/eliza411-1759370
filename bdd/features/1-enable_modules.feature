Feature: Add users and roles
  In order to delegate content publishing
  As a site builder
  I need to add roles and users

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario: Create roles
    When I follow "Modules"
    And I check "Chaos tools"
    And I check "Views"
    And I check "Views UI"
    And I check "Libraries"
    And I check "Diff"
    And I check "Views Slideshow"
    And I check "Pathauto"
    And I check "Token"
    And I check "Devel"
    And I press "Save configuration"
