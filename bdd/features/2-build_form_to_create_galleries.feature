Feature: Build the form for creating galleries
  In order to delegate content publishing
  As a site builder
  I need to build the form to create galleries

  Background:
  Given I am on "/"
  And I fill in "Username" with "admin"
  And I fill in "Password" with "alepc12"
  And I press "Log in"

  Scenario:Add a new content type called galleries 
    When I follow "Structure"
    And I follow "Content types"
    And I follow "Add content type"
    And I fill in "Name" with "Gallery"
    And I fill in "Description" with "Create an image gallery with slideshow controls."
    And I fill in "Title field label" with "gallery name"
    And I uncheck "Promoted to front page"
    And I check "Published"
    And I uncheck "Display author and date information"
    And I select "Closed" from "Default comment setting for new content"
    And I uncheck "Main menu"
    And I press "Save content type"

  Scenario: Add an image field
    When I follow "Structure"
    And I follow "Content types"
    And I follow "http://slideshow.l/?q=admin/structure/types/manage/gallery/fields"

  Scenario: Create proper paths
    When I follow "Configuration"
    And I follow "URL aliases"
    And I follow "Patterns"
    And I fill in "Pattern for all Gallery paths" with "galleries/[node:title]"
    And I press "Save configuration"
